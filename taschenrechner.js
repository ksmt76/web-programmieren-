lator = /** @class */ (function () {
    function Calculator() {
        this.number1 = 0;
        this.number2 = 0;
        this.addButtons();
        this.addValueListener();
        this.getOutput();
    }
    Calculator.prototype.addButtons = function () {
        var _this = this;
        var btn = document.getElementById("sum");
        btn.addEventListener("click", function () { _this.sum(); });
        btn = document.getElementById("diff");
        btn.addEventListener("click", function () { _this.diff(); });
        btn = document.getElementById("mult");
        btn.addEventListener("click", function () { _this.multiply(); });
        btn = document.getElementById("quot");
        btn.addEventListener("click", function () { _this.quotient(); });
        btn = document.getElementById("area");
        btn.addEventListener("click", function () { _this.multiply(); });
        btn = document.getElementById("extent");
        btn.addEventListener("click", function () { _this.extent(); });
        btn = document.getElementById("power");
        btn.addEventListener("click", function () { _this.power(); });
        btn = document.getElementById("circle_area");
        btn.addEventListener("click", function () { _this.circle_area(); });
        btn = document.getElementById("circle_extent");
        btn.addEventListener("click", function () { _this.circle_extent(); });
        btn = document.getElementById("fakulty");
        btn.addEventListener("click", function () { _this.fakulty(); });
        btn = document.getElementById("euler");
        btn.addEventListener("click", function () { _this.euler(); });
        btn = document.getElementById("mod");
        btn.addEventListener("click", function () { _this.mod(); });
        btn = document.getElementById("horner");
        btn.addEventListener("click", function () { _this.horner(); });
    };
    Calculator.prototype.addButton = function (name, func) {
        var btn = document.getElementById("sum");
        btn.addEventListener("click", function () { return func; });
    };
    Calculator.prototype.addValueListener = function () {
        var _this = this;
        var num1 = document.getElementById("num1");
        num1.addEventListener("change", function () {
            _this.number1 = Number(num1.value);
        });
        var num2 = document.getElementById("num2");
        num2.addEventListener("change", function () {
            _this.number2 = Number(num2.value);
        });
    };
    Calculator.prototype.getOutput = function () {
        this.output = document.getElementById("myoutput");
    };
    Calculator.prototype.sum = function () {
        this.output.textContent = String(this.number1 + this.number2);
    };
    Calculator.prototype.diff = function () {
        this.output.textContent = String(this.number1 - this.number2);
    };
    Calculator.prototype.multiply = function () {
        this.output.textContent = String(this.number1 * this.number2);
    };
    Calculator.prototype.quotient = function () {
        this.output.textContent = String(this.number1 / this.number2);
    };
    Calculator.prototype.extent = function () {
        this.output.textContent = String(2 * (this.number1 + this.number2));
    };
    Calculator.prototype.power = function () {
        this.output.textContent = String(Math.pow(this.number1, this.number2));
    };
    Calculator.prototype.circle_area = function () {
        this.output.textContent = String(Math.PI * this.number1 * this.number2);
    };
    Calculator.prototype.circle_extent = function () {
        this.output.textContent = String(2 * Math.PI * this.number1);
    };
    Calculator.prototype.fak = function (n, f) {
        if (f === void 0) { f = 1; }
        if (n > 1) {
            return this.fak(n - 1, f * n);
        }
        return f;
    };
    Calculator.prototype.fakulty = function () {
        this.output.textContent = String(this.fak(this.number1));
    };
    Calculator.prototype.euler = function () {
        this.output.textContent = String(Math.exp(this.number1));
    };
    Calculator.prototype.mod = function () {
        this.output.textContent = String(this.number1 % this.number2);
    };
    Calculator.prototype.to_dual = function (n, m, run) {
        if (m === void 0) { m = 0; }
        if (run === void 0) { run = 0; }
        m = (n % 2) * (Math.pow(10, run)) + m;
        if (n <= 1) {
            return m;
        }
        return this.to_dual(~~(n / 2), m, run + 1);
    };
    Calculator.prototype.horner = function () {
        this.output.textContent = String(this.to_dual(this.number1));
    };
    return Calculator;
}());
window.onload = function () { return new Calculator(); };
